# JWTdown for FastAPI

This is an easy-to-use authentication mechanism for FastAPI.

It draws inspiration from the tutorials found in the FastAPI
documentation.

Please read the
[documentation](https://jwtdown-fastapi.readthedocs.io/en/latest/intro.html)
to use this project.

## Developing

Development on this project is limited to employees,
contractors, and students of Galvanize, Inc.

